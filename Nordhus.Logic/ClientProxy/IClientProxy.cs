﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Nordhus.Logic.ServiceReference1;

namespace Nordhus.Logic
{
    public interface IClientProxy : INordhusDataService, ICommunicationObject
    {
    }
}
