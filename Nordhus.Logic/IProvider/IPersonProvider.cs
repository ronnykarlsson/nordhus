﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;

namespace Nordhus.Logic
{
    public interface IPersonProvider : IDisposable
    {
        Person Get(int personId);
        List<Person> GetAll();
        List<Person> Search(string searchString);
    }
}
