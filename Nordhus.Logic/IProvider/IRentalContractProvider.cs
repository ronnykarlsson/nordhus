﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;

namespace Nordhus.Logic
{
    public interface IRentalContractProvider : IDisposable
    {
        RentalContract Get(int contractId);
        List<RentalContract> GetAll();
        List<RentalContract> Search(string searchString);
        void CreateContract(RentalContract contract);
        void UpdateContract(RentalContract contract);
        void CloseContract(int contractId);
    }
}
