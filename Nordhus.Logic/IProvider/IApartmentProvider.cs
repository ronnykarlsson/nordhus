﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;

namespace Nordhus.Logic
{
    public interface IApartmentProvider : IDisposable
    {
        Apartment Get(int RoomId);
        List<Apartment> GetAll();
        List<Apartment> GetAvailable();
        List<Apartment> GetNotAvailable();
        List<Apartment> Search(string searchString);
    }
}
