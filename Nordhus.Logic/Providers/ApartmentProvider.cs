﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;
using System.ServiceModel;
using Nordhus.Logic.ServiceReference1;

namespace Nordhus.Logic
{
    public class ApartmentProvider : IApartmentProvider
    {
        IClientProxy client;

        public ApartmentProvider()
            : this(new NordhusDataServiceClient())
        {
        }

        public ApartmentProvider(IClientProxy client)
        {
            this.client = client;
        }

        public Apartment Get(int apartmentId)
        {
            if (apartmentId < 0)
                throw new InvalidOperationException("Invalid apartmentId");

            return client.GetApartmentById(apartmentId);
        }

        public List<Apartment> GetAll()
        {
            return client.GetAllApartments();
        }

        public List<Apartment> GetAvailable()
        {
            return client.GetAvailableApartments();
        }

        public List<Apartment> GetNotAvailable()
        {
            return client.GetNotAvailableApartments();
        }

        public List<Apartment> Search(string searchString)
        {
            throw new NotImplementedException();
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            if (client != null && client.State == CommunicationState.Opened)
            {
                client.Close();
            }
        }

        ~ApartmentProvider()
        {
            Dispose(false);
        }
        #endregion
    }
}
