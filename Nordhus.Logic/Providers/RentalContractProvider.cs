﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;
using System.ServiceModel;
using Nordhus.Logic.ServiceReference1;

namespace Nordhus.Logic
{
    public class RentalContractProvider : IRentalContractProvider
    {
        IClientProxy client;

        public RentalContractProvider()
            :this(new NordhusDataServiceClient())
        {
        }

        public RentalContractProvider(IClientProxy client)
        {
            this.client = client;
        }

        public RentalContract Get(int contractId)
        {
            if (contractId < 0)
                throw new InvalidOperationException("Invalid contractId");

            return client.GetContractById(contractId);
        }

        public List<RentalContract> GetAll()
        {
            return client.GetAllContracts();
        }

        public List<RentalContract> Search(string searchString)
        {
            throw new NotImplementedException();
        }

        public void CreateContract(RentalContract contract)
        {
            if (contract == null)
                throw new ArgumentNullException("contract");

            if (contract.RentalContractId >= 0)
                throw new InvalidOperationException("Invalid RentalContractId for a new contract");

            ValidationResult result = contract.IsValid();
            if (!result.IsValid)
                throw new InvalidOperationException("Invalid contract parameters");

            contract.RentalContractId = client.CreateContract(contract);
        }

        public void UpdateContract(RentalContract contract)
        {
            if (contract == null)
                throw new ArgumentNullException("contract");

            if (contract.RentalContractId < 0)
                throw new InvalidOperationException("Invalid RentalContractId");

            ValidationResult result = contract.IsValid();
            if (!result.IsValid)
                throw new InvalidOperationException("Invalid contract parameters");

            client.UpdateContract(contract);
        }

        public void CloseContract(int contractId)
        {
            throw new NotImplementedException();
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            if (client != null && client.State == CommunicationState.Opened)
            {
                client.Close();
            }
        }

        ~RentalContractProvider()
        {
            Dispose(false);
        }
        #endregion
    }
}
