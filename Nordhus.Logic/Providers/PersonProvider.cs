﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;
using System.ServiceModel;
using Autofac;
using Nordhus.Logic.ServiceReference1;

namespace Nordhus.Logic
{
    public class PersonProvider : IPersonProvider
    {
        IClientProxy client;

        public PersonProvider()
            : this(new NordhusDataServiceClient())
        {
        }

        public PersonProvider(IClientProxy client)
        {
            this.client = client;
        }

        public Person Get(int personId)
        {
            if (personId < 0)
                throw new InvalidOperationException("Invalid PersonId");

            return client.GetPersonById(personId);
        }

        public List<Person> GetAll()
        {
            return client.GetAllPeople();
        }

        public List<Person> Search(string searchString)
        {
            throw new NotImplementedException();
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            if (client != null && client.State == CommunicationState.Opened)
            {
                client.Close();
            }
        }

        ~PersonProvider()
        {
            Dispose(false);
        }
        #endregion
    }
}
