﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Nordhus.Domain;
using Nordhus.Logic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Diagnostics;
using System.Threading;

namespace Nordhus
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public readonly App Nordhus = (App)(App.Current);

        int loadCounter;
        private static readonly object syncLoading = new object();

        private ApartmentFilterOptions apartmentFilterOptions;

        public MainWindow()
        {
            InitializeComponent();

            loadCounter = 0;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            apartmentFilterOptions = ApartmentFilterOptions.Available;

            Task.Factory.StartNew(() =>
                {
                    LoadApartments();
                });

            Task.Factory.StartNew(() =>
                {
                    LoadRentalContracts();
                });

            e.Handled = true;
        }

        private void LoadApartments()
        {
            LoadStatusBegin();

            List<Apartment> apartmentList = null;

            // Load apartments
            using (var provider = Nordhus.ApartmentProvider)
            {
                try
                {
                    if (apartmentFilterOptions == ApartmentFilterOptions.Available) apartmentList = provider.GetAvailable();
                    else if (apartmentFilterOptions == ApartmentFilterOptions.NotAvailable) apartmentList = provider.GetNotAvailable();
                    else apartmentList = provider.GetAll();
                }
                catch (FaultException ex)
                {
                    DisplayStatus(ex.Message);
                    LoadStatusEnd();
                    return;
                }
                catch (CommunicationException ex)
                {
                    DisplayStatus("Unable to contact server, make sure NordhusServiceConsole is running.");
                    LoadStatusEnd();
                    return;
                }
            }

            // Show apartments for the user
            Dispatcher.Invoke(new Action(() =>
                {
                    // Create new apartment collection if this is the first call
                    if (ApartmentsGrid.DataContext == null)
                    {
                        ApartmentsGrid.DataContext = new ObservableCollection<Apartment>();
                    }

                    ObservableCollection<Apartment> apartments = (ObservableCollection<Apartment>)ApartmentsGrid.DataContext;
                    apartments.Clear();

                    foreach (var apartment in apartmentList)
                    {
                        apartments.Add(apartment);
                    }
                    ApartmentListBox.SelectedIndex = 0;

                    LoadStatusEnd();
                }));
        }

        private void LoadRentalContracts()
        {
            LoadStatusBegin();

            // Load rental contracts from server
            List<RentalContract> contractList = null;
            using (var provider = Nordhus.ContractProvider)
            {
                try
                {
                    contractList = provider.GetAll();
                }
                catch (FaultException ex)
                {
                    DisplayStatus(ex.Message);
                    LoadStatusEnd();
                    return;
                }
                catch (CommunicationException ex)
                {
                    DisplayStatus("Unable to contact server, make sure NordhusServiceConsole is running.");
                    LoadStatusEnd();
                    return;
                }
            }

            // Display contracts for the user
            Dispatcher.Invoke(new Action(() =>
            {
                if (RentalContractsGrid.DataContext == null)
                {
                    RentalContractsGrid.DataContext = new ObservableCollection<RentalContract>();
                }

                ObservableCollection<RentalContract> rentalContracts = (ObservableCollection<RentalContract>)RentalContractsGrid.DataContext;
                rentalContracts.Clear();

                foreach (var contract in contractList)
                {
                    rentalContracts.Add(contract);
                }

                LoadStatusEnd();
            }));
        }

        /// <summary>
        /// Shows loading status on screen
        /// </summary>
        private void LoadStatusBegin()
        {
            lock (syncLoading)
            {
                loadCounter++;

                // When the counter hits 1, display a loading indiciator
                // Greater than 1 means that it's already showing
                if (loadCounter == 1)
                {
                    Dispatcher.Invoke(new Action(() =>
                        {
                            OverlayGrid.Visibility = Visibility.Visible;
                        }));
                }
            }
        }

        /// <summary>
        /// Ends loading status
        /// </summary>
        private void LoadStatusEnd()
        {
            lock (syncLoading)
            {
                loadCounter--;
                Debug.Assert(loadCounter >= 0);

                // If counter is 0, reset loading.
                // Otherwise there are more running operations as well which the user must wait for
                if (loadCounter == 0)
                {
                    Dispatcher.Invoke(new Action(() =>
                        {
                            OverlayGrid.Visibility = Visibility.Collapsed;
                        }));
                }
            }
        }

        private void ApartmentFilterButton_Click(object sender, RoutedEventArgs e)
        {
            ApartmentFilteringContextMenu.PlacementTarget = this;
            ApartmentFilteringContextMenu.IsOpen = true;

            e.Handled = true;
        }

        private void ContractsFilterButton_Click(object sender, RoutedEventArgs e)
        {
            ContractsFilteringContextMenu.PlacementTarget = this;
            ContractsFilteringContextMenu.IsOpen = true;

            e.Handled = true;
        }

        private void ApartmentCreateContractButton_Click(object sender, RoutedEventArgs e)
        {
            // Don't create a contract if there is a running contract already
            Apartment apartment = ApartmentListBox.SelectedValue as Apartment;
            if (apartment == null || apartment.CurrentContract != null)
            {
                DisplayStatus("There is an active contract for this apartment.");
                return;
            }

            DisableUI(ApartmentCreateGrid);

            // Set default control values
            StartDatePicker.SelectedDate = DateTime.Now;
            EndDatePicker.SelectedDate = null;
            CreateContractPeopleListBox.DataContext = new ObservableCollection<Person>();

            e.Handled = true;
        }

        /// <summary>
        /// Disable and hide all UI elements except for <paramref name="element"/>
        /// </summary>
        /// <param name="element">The element that should not be hidden</param>
        private void DisableUI(FrameworkElement element)
        {
            Dispatcher.Invoke(new Action(() =>
                {
                    ApartmentListBox.IsEnabled = false;

                    ApartmentFilterButton.IsEnabled = false;
                    ApartmentCreateContractButton.IsEnabled = false;
                    EditApartmentContractButton.IsEnabled = false;
                    CloseApartmentContractButton.IsEnabled = false;

                    ContractEditButton.IsEnabled = false;

                    ContractsViewGrid.Visibility = Visibility.Collapsed;

                    ApartmentViewGrid.Visibility = Visibility.Collapsed;
                    ApartmentEditGrid.Visibility = Visibility.Collapsed;
                    ApartmentCloseGrid.Visibility = Visibility.Collapsed;

                    if (element != null)
                    {
                        element.Visibility = Visibility.Visible;
                    }
                }));
        }

        private void EnableUI()
        {
            Dispatcher.Invoke(new Action(() =>
                {
                    ApartmentListBox.IsEnabled = true;

                    ApartmentFilterButton.IsEnabled = true;
                    ApartmentCreateContractButton.IsEnabled = true;
                    EditApartmentContractButton.IsEnabled = true;
                    CloseApartmentContractButton.IsEnabled = true;

                    ContractEditButton.IsEnabled = true;

                    ContractsEditGrid.Visibility = Visibility.Collapsed;
                    ContractsViewGrid.Visibility = Visibility.Visible;

                    ApartmentCreateGrid.Visibility = Visibility.Collapsed;
                    ApartmentEditGrid.Visibility = Visibility.Collapsed;
                    ApartmentCloseGrid.Visibility = Visibility.Collapsed;
                    ApartmentViewGrid.Visibility = Visibility.Visible;
                }));
        }

        private void ApartmentCreateContractDoneButton_Click(object sender, RoutedEventArgs e)
        {
            // Send contract to database
            Apartment apartment = ApartmentListBox.SelectedValue as Apartment;
            if (apartment == null) return;

            LoadStatusBegin();

            List<Person> people = new List<Person>();

            foreach (Person person in CreateContractPeopleListBox.Items)
            {
                people.Add(person);
            }

            if (people.Count == 0)
            {
                DisplayStatus("No people selected for the contract.");
                LoadStatusEnd();
                return;
            }

            RentalContract contract = new RentalContract(-1, apartment.ApartmentId, people, (DateTime)StartDatePicker.SelectedDate, EndDatePicker.SelectedDate);

            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        using (var provider = Nordhus.ContractProvider)
                        {
                            provider.CreateContract(contract);
                        }

                        Dispatcher.Invoke(new Action(EnableUI));

                        LoadRentalContracts();
                    }
                    catch (FaultException ex)
                    {
                        DisplayStatus(ex.Message);
                        LoadStatusEnd();
                        return;
                    }

                    LoadStatusEnd();
                });
        }

        private void ApartmentCreateContractCancelButton_Click(object sender, RoutedEventArgs e)
        {
            EnableUI();
        }

        private void SearchPersonButton_Click(object sender, RoutedEventArgs e)
        {
            LoadStatusBegin();

            CreateContractPeopleListBox.Visibility = Visibility.Collapsed;
            SearchPeopleListBox.Visibility = Visibility.Visible;

            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        ObservableCollection<Person> searchResults;

                        using (var provider = Nordhus.PersonProvider)
                        {
                            // TODO: Implement search functionality
                            searchResults = new ObservableCollection<Person>(provider.GetAll());
                        }

                        Dispatcher.Invoke(new Action(() =>
                            {
                                SearchPeopleListBox.DataContext = searchResults;
                            }));

                        LoadRentalContracts();
                    }
                    catch (FaultException ex)
                    {
                        DisplayStatus(ex.Message);
                        LoadStatusEnd();
                        return;
                    }

                    LoadStatusEnd();
                });
        }

        private void SearchPeopleListBox_Click(object sender, RoutedEventArgs e)
        {
            Button button = e.OriginalSource as Button;
            if (button == null) return;

            Person selectedPerson = (Person)button.Tag;

            var personCollection = (ObservableCollection<Person>)CreateContractPeopleListBox.DataContext;

            // Add person to renter collection
            if (!personCollection.Contains(selectedPerson))
            {
                personCollection.Add(selectedPerson);
            }

            SearchPeopleListBox.Visibility = Visibility.Collapsed;
            CreateContractPeopleListBox.Visibility = Visibility.Visible;
            e.Handled = true;
        }

        private void EditApartmentButton_Click(object sender, RoutedEventArgs e)
        {
            Apartment apartment = ApartmentListBox.SelectedValue as Apartment;
            if (apartment == null || apartment.CurrentContract == null)
            {
                DisplayStatus("No contract selected");
                return;
            }

            // Initialize controls
            EditStartDatePicker.SelectedDate = apartment.CurrentContract.StartDate;
            EditEndDatePicker.SelectedDate = apartment.CurrentContract.EndDate;

            DisableUI(ApartmentEditGrid);

            e.Handled = true;
        }

        private void ApartmentEditContractDoneButton_Click(object sender, RoutedEventArgs e)
        {
            EnableUI();

            e.Handled = true;
        }

        private void ApartmentEditContractCancelButton_Click(object sender, RoutedEventArgs e)
        {
            EnableUI();

            e.Handled = true;
        }

        private void ContractEditButton_Click(object sender, RoutedEventArgs e)
        {
            RentalContract contract = RentalContractsListBox.SelectedValue as RentalContract;
            if (contract == null) return;

            ContractEditStartDatePicker.SelectedDate = contract.StartDate;
            ContractEditEndDatePicker.SelectedDate = contract.EndDate;

            DisableUI(ContractsEditGrid);

            e.Handled = true;
        }

        private void ContractEditContractDoneButton_Click(object sender, RoutedEventArgs e)
        {
            RentalContract contract = RentalContractsListBox.SelectedValue as RentalContract;
            if (contract == null) return;

            LoadStatusBegin();

            if (ContractEditStartDatePicker.SelectedDate == null)
            {
                DisplayStatus("Start date is missing");
                LoadStatusEnd();
                e.Handled = true;
                return;
            }

            contract.StartDate = (DateTime)ContractEditStartDatePicker.SelectedDate;
            contract.EndDate = ContractEditEndDatePicker.SelectedDate;

            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        using (var provider = Nordhus.ContractProvider)
                        {
                            provider.UpdateContract(contract);
                        }

                        EnableUI();
                    }
                    catch (FaultException ex)
                    {
                        DisplayStatus(ex.Message);
                        LoadStatusEnd();
                        return;
                    }

                    LoadStatusEnd();
                });

            e.Handled = true;
        }

        private void ContractEditContractCancelButton_Click(object sender, RoutedEventArgs e)
        {
            EnableUI();

            e.Handled = true;
        }

        private void ShowAllApartmentsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
                {
                    apartmentFilterOptions = ApartmentFilterOptions.All;
                    LoadApartments();
                });

            e.Handled = true;
        }

        private void ShowAvailableApartmentsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
                {
                    apartmentFilterOptions = ApartmentFilterOptions.Available;
                    LoadApartments();
                });

            e.Handled = true;
        }

        private void ShowRentedApartmentsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
                {
                    apartmentFilterOptions = ApartmentFilterOptions.NotAvailable;
                    LoadApartments();
                });

            e.Handled = true;
        }

        private void CloseApartmentContractButton_Click(object sender, RoutedEventArgs e)
        {
            var apartment = ApartmentListBox.SelectedValue as Apartment;
            if (apartment == null || apartment.CurrentContract == null || apartment.CurrentContract.EndDate != null)
            {
                DisplayStatus("There is no active contract.");
                return;
            }

            DisableUI(ApartmentCloseGrid);

            CloseContractDatePicker.SelectedDate = DateTime.Now;

            e.Handled = true;
        }

        private void ApartmentCloseContractDoneButton_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
                {
                    LoadStatusBegin();

                    try
                    {
                        using (var provider = Nordhus.ContractProvider)
                        {
                            var apartment = ApartmentListBox.SelectedValue as Apartment;
                            var contract = apartment.CurrentContract as RentalContract;

                            contract = provider.Get(contract.RentalContractId);
                            contract.EndDate = CloseContractDatePicker.SelectedDate;

                            provider.UpdateContract(contract);
                        }

                        EnableUI();

                        LoadApartments();
                    }
                    catch (FaultException ex)
                    {
                        DisplayStatus(ex.Message);
                        LoadStatusEnd();
                        return;
                    }

                    LoadStatusEnd();
                });

            e.Handled = true;
        }

        private void ApartmentCloseContractCancelButton_Click(object sender, RoutedEventArgs e)
        {
            EnableUI();

            e.Handled = true;
        }

        private void DisplayStatus(string message)
        {
            MessageBox.Show(message);
        }
    }
}
