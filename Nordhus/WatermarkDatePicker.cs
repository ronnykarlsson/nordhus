﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

namespace Nordhus
{
    public class WatermarkDatePicker : DatePicker
    {
        protected static ContentControl WatermarkContent;

        public WatermarkDatePicker()
            : base()
        {
            this.Loaded += new RoutedEventHandler(WatermarkDatePicker_Loaded);
        }

        void WatermarkDatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            var datePickerTextBox = GetChildOfType<DatePickerTextBox>(this);
            if (datePickerTextBox == null) return;

            var watermark = datePickerTextBox.Template.FindName("PART_Watermark", datePickerTextBox) as ContentControl;
            if (watermark == null) return;

            WatermarkContent = watermark;

            watermark.Content = Watermark;
        }

        static void WatermarkDatePicker_WatermarkChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (WatermarkContent == null) return;

            WatermarkContent.Content = e.NewValue as string;
        }

        public static T GetChildOfType<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }
            return null;
        }

        public string Watermark
        {
            get { return (string)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Watermark.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WatermarkProperty =
            DependencyProperty.Register("Watermark", typeof(string), typeof(WatermarkDatePicker), new UIPropertyMetadata(new PropertyChangedCallback(WatermarkDatePicker_WatermarkChanged)));
    }
}
