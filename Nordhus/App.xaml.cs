﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Nordhus.Logic;
using Autofac;

namespace Nordhus
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        IContainer container;

        public App()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<PersonProvider>().As<IPersonProvider>();
            builder.RegisterType<ApartmentProvider>().As<IApartmentProvider>();
            builder.RegisterType<RentalContractProvider>().As<IRentalContractProvider>();
            container = builder.Build();
        }

        public IPersonProvider PersonProvider { get { return container.Resolve<IPersonProvider>(); } }
        public IApartmentProvider ApartmentProvider { get { return container.Resolve<IApartmentProvider>(); } }
        public IRentalContractProvider ContractProvider { get { return container.Resolve<IRentalContractProvider>(); } }
    }
}
