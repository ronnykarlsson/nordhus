﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nordhus
{
    public enum ApartmentFilterOptions
    {
        All,
        Available,
        NotAvailable
    }
}
