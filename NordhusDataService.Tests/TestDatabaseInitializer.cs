﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using NordhusDataService.DAL;
using Nordhus.Domain;

namespace NordhusDataService.Tests
{
    public class TestDatabaseInitializer : DropCreateDatabaseAlways<NordhusContext>
    {
        protected override void Seed(NordhusContext context)
        {
            List<Person> personList = new List<Person>();
            List<Apartment> apartmenstList = new List<Apartment>();
            List<RentalContract> contractList = new List<RentalContract>();

            // Person objects for initialization
            personList.Add(new Person(0, "John", "Doe", "706050-4030"));
            personList.Add(new Person(1, "Jane", "Doe", "605040-3020"));

            // Addresses to build apartments out of
            Address address1 = new Address("Little Street 5", 12343, "Big City", "Uptown");
            Address address2 = new Address("Queens Street 10", 23432, "Las Vegas", "Downtown");
            Address address3 = new Address("Kings Street 10", 13432, "Las Vegas", null);

            // Apartments for initialization
            int apartmentId = 0;
            for (int i = 0; i < 7; i++) apartmenstList.Add(new Apartment(apartmentId++, 1000 + i, address1));
            for (int i = 0; i < 9; i++) apartmenstList.Add(new Apartment(apartmentId++, 1000 + i, address2));
            for (int i = 0; i < 4; i++) apartmenstList.Add(new Apartment(apartmentId++, 1000 + i, address3));

            // Add to database
            personList.ForEach(o => context.People.Add(o));
            apartmenstList.ForEach(o => context.Apartments.Add(o));
            contractList.ForEach(o => context.Contracts.Add(o));

            base.Seed(context);
        }
    }
}
