﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NordhusDataService.DAL;
using System.Data.Entity;
using Nordhus.Domain;
using System.ServiceModel;
using Autofac;

namespace NordhusDataService.Tests
{
    [TestFixture]
    public class NordhusDataService_Tests
    {
        NordhusDataService service;
        ContainerBuilder builder;
        IContainer container;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            builder = new ContainerBuilder();
            builder.RegisterType<NordhusDataContext>().As<IDataAccessLayer>();
            container = builder.Build();

            // Initialize database with the same test data each time
            service = new NordhusDataService();
            Database.SetInitializer<NordhusContext>(new TestDatabaseInitializer());
        }

        [SetUp]
        public void NordhusDataServiceSetUp()
        {
            // Delete previous database to be sure we start up with a fresh one
            using (NordhusContext context = new NordhusContext())
            {
                context.Database.Delete();
                context.Database.Initialize(true);
            }
        }

        [Test]
        public void GetPersonById2RetrievesJane()
        {
            Person jane = service.GetPersonById(2);
            StringAssert.AreEqualIgnoringCase("Jane", jane.FirstName);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void GetPersonByInvalidIdGivesFaultException()
        {
            service.GetPersonById(-1);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void GetPersonByNotFoundIdGivesFaultException()
        {
            service.GetPersonById(int.MaxValue);
        }

        [Test]
        public void GetAllPeopleRetrievesTwoPeople()
        {
            var people = service.GetAllPeople();
            Assert.AreEqual(2, people.Count);
        }

        [Test]
        public void GetAllPeopleRetrievesJane()
        {
            var people = service.GetAllPeople();
            var jane = people.First( p => p.FirstName.Equals("Jane"));
            Assert.AreEqual("Jane", jane.FirstName);
        }

        [Test]
        public void GetAllPeoplesHasNoDuplicates()
        {
            var people = service.GetAllPeople();
            CollectionAssert.AllItemsAreUnique(people);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void GetApartmentByInvalidIdGivesFaultException()
        {
            service.GetApartmentById(-1);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void GetApartmentByNotFoundIdGivesFaultException()
        {
            service.GetApartmentById(int.MaxValue);
        }

        [Test]
        public void GetApartmentById1GivesBigCityApartment()
        {
            var apartment = service.GetApartmentById(1);
            Assert.AreEqual("Big City", apartment.Address.City);
        }

        [Test]
        public void GetAllApartmentsHasNoDuplicates()
        {
            var apartments = service.GetAllApartments();
            CollectionAssert.AllItemsAreUnique(apartments);
        }

        [Test]
        public void GetAllApartmentsFindsDowntownAddresses()
        {
            var apartments = service.GetAllApartments();
            var apartment = apartments.First(o => o.Address.Area.Equals("Downtown"));
            Assert.IsNotNull(apartment);
        }

        [Test]
        public void GetAvailableApartmentsOnlyRetrievesAvailableApartments()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, null);
            contract.RentalContractId = service.CreateContract(contract);

            var apartments = service.GetAvailableApartments();
            foreach (var apartment in apartments)
                foreach (var tempContract in apartment.Contracts)
                    Assert.AreEqual(null, tempContract.EndDate);
        }

        [Test]
        public void GetAvailableApartmentsDoesntRetrieveApartmentsWithContract()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, null);
            contract.RentalContractId = service.CreateContract(contract);

            var apartments = service.GetAvailableApartments();
            Assert.IsTrue(apartments.FirstOrDefault(o => o.ApartmentId == 1) == null);
        }

        [Test]
        public void GetNotAvailableApartmentsDoesntRetrieveApartmentsWithoutContracts()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, null);
            contract.RentalContractId = service.CreateContract(contract);

            var apartments = service.GetNotAvailableApartments();
            Assert.IsFalse(apartments.FirstOrDefault(o => o.ApartmentId == 1) == null);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void GetContractByInvalidIdGivesFaultException()
        {
            service.GetContractById(-1);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void GetContractByNotFoundIdGivesFaultException()
        {
            service.GetContractById(int.MaxValue);
        }

        [Test]
        public void GetAllContractsReturnsCollectionWithoutErrors()
        {
            ICollection<RentalContract> contracts = service.GetAllContracts();
            Assert.IsNotNull(contracts);
        }

        [Test]
        public void CreateContractReturnsIdOfNewContract()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            int id = service.CreateContract(contract);
            Assert.GreaterOrEqual(id, 0);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractDoesNotAllowEndDatesFromBeforeStartDate()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(-30));
            service.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractStartDateAndEndDateCantBeSame()
        {
            DateTime now = DateTime.UtcNow;
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, now, now);
            service.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractFailsWithNoRenters()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            service.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractFailsWithBadPersonArgument()
        {
            var contract = new RentalContract(-1, 1, null, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            service.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractRequiresExistingApartment()
        {
            var contract = new RentalContract(-1, int.MaxValue, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            service.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractRequiresExistingPerson()
        {
            Person person = new Person(int.MaxValue, "Mr", "X", "102030-4050");
            var contract = new RentalContract(-1, 1, new List<Person> { person }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            service.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractOnlyTakesNegativeContractIds()
        {
            var contract = new RentalContract(0, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            service.CreateContract(contract);
        }

        [Test]
        public void CreatedContractIsPersistedAndRetrievedCorrectly()
        {
            var contract1 = CreateContractHelper();
            var contract2 = service.GetContractById(contract1.RentalContractId);
            Assert.IsTrue(contract1.Equals(contract2));
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractsWithOverlappingDatesNoEndDate()
        {
            var contract1 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, null);
            var contract2 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, contract1.StartDate, null);
            service.CreateContract(contract1);
            service.CreateContract(contract2);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractsWithOverlappingDatesWithEnding()
        {
            var contract1 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            var contract2 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, contract1.StartDate.AddDays(1), null);
            service.CreateContract(contract1);
            service.CreateContract(contract2);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void CreateContractsWithOverlappingDatesWithBadEndDate()
        {
            var contract1 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            var contract2 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, contract1.StartDate.AddDays(-1), contract1.StartDate.AddDays(1));
            service.CreateContract(contract1);
            service.CreateContract(contract2);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void UpdateContractWithInvalidIdGivesFaultException()
        {
            var contract = CreateContractHelper();
            contract.RentalContractId = -1;
            service.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void UpdateNonExistingContractGivesFaultException()
        {
            var contract = CreateContractHelper();
            contract.RentalContractId = int.MaxValue;
            service.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void UpdateContractDoesntAllowStartDateAndDateToBeSame()
        {
            var contract = service.GetContractById(1);
            contract.EndDate = contract.StartDate;
            service.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void UpdateContractRequiresEndDateGreaterThanStartDate()
        {
            var contract = service.GetContractById(1);
            contract.EndDate = contract.StartDate.AddDays(-1);
            service.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void UpdateContractDoesntAllowChangingContractOwner()
        {
            var contract = service.GetContractById(1);
            contract.Renter.First().PersonId++;
            service.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void UpdateContractDoesntAllowChangingRentercount()
        {
            var contract = service.GetContractById(1);
            contract.Renter.Add(new Person(2, "Another", "Person", "102030-4050"));
            service.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void UpdateContractDoesntAllowChangingApartment()
        {
            var contract = service.GetContractById(1);
            contract.ApartmentId++;
            service.UpdateContract(contract);
        }

        [Test]
        public void UpdateContractChangesArePersistedAndCanBeRetrieved()
        {
            CreateContractHelper();
            var contract1 = service.GetContractById(1);
            contract1.StartDate = contract1.StartDate.AddDays(1);
            contract1.EndDate = contract1.EndDate.Value.AddDays(1);
            service.UpdateContract(contract1);
            var contract2 = service.GetContractById(1);
            Assert.IsTrue(contract1.Equals(contract2));
        }

        [Test]
        [ExpectedException(typeof(FaultException))]
        public void UpdateContractsWithOverlappingDatesNoEndDate()
        {
            var contract1 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(10));
            var contract2 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, contract1.StartDate, null);
            var contract3 = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, contract1.StartDate, null);
            contract2.RentalContractId = service.CreateContract(contract1);
            contract3.RentalContractId = service.CreateContract(contract3);
            service.UpdateContract(contract2);
        }

        private RentalContract CreateContractHelper()
        {
            RentalContract contract;
            contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            contract.RentalContractId = service.CreateContract(contract);
            return contract;
        }

        private Person GetPersonHelper()
        {
            return service.GetPersonById(1);
        }
    }
}
