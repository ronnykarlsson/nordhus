﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NordhusDataService.DAL
{
    public interface IDataAccessLayer : IUnitOfWork
    {
        IPersonRepository People { get; }
        IApartmentRepository Apartments { get; }
        IRentalContractRepository RentalContracts { get; }
    }
}
