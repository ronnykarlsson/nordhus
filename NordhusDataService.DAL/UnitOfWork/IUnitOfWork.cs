﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NordhusDataService.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        void SaveChanges();
    }
}
