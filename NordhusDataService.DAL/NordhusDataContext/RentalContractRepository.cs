﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;

namespace NordhusDataService.DAL
{
    public class RentalContractRepository : Repository<RentalContract>, IRentalContractRepository
    {
        public RentalContractRepository(NordhusContext context)
            : base(context)
        {
        }

        public RentalContract FindById(int id)
        {
            try
            {
                return context.Contracts.Include("Renter").Where(o => o.RentalContractId == id).First();
            }
            catch (InvalidOperationException)
            {
                // InvalidOperationException most likely means that the contract wasn't found
                // Signal errors by returning null
                return null;
            }
        }

        public override void Create(RentalContract entity)
        {
            foreach (Person person in entity.Renter)
            {
                context.People.Attach(person);
            }
            context.Contracts.Add(entity);
        }

        public override List<RentalContract> Read()
        {
            return context.Contracts.Include("Renter").Include("Apartment").ToList();
        }
    }
}
