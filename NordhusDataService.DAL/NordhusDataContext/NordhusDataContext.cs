﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NordhusDataService.DAL
{
    public class NordhusDataContext : IDataAccessLayer
    {
        private NordhusContext context;
        private IPersonRepository people;
        private IApartmentRepository apartments;
        private IRentalContractRepository rentalContracts;

        public NordhusDataContext()
        {
            context = new NordhusContext();
        }

        public IPersonRepository People
        {
            get
            {
                if (people == null)
                {
                    people = new PersonRepository(context);
                }
                return people;
            }
        }

        public IApartmentRepository Apartments
        {
            get
            {
                if (apartments == null)
                {
                    apartments = new ApartmentRepository(context);
                }
                return apartments;
            }
        }

        public IRentalContractRepository RentalContracts
        {
            get
            {
                if (rentalContracts == null)
                {
                    rentalContracts = new RentalContractRepository(context);
                }
                return rentalContracts;
            }
        }

        public void SaveChanges()
        {
            if (context != null)
            {
                context.SaveChanges();
            }
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            if (people != null) people.Dispose();
            if (apartments != null) apartments.Dispose();
            if (rentalContracts != null) rentalContracts.Dispose();
            context.Dispose();
        }

        ~NordhusDataContext()
        {
            Dispose(false);
        } 
        #endregion
    }
}
