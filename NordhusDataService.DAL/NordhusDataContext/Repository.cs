﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;

namespace NordhusDataService.DAL
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        protected NordhusContext context;
        private bool sharedContext;

        protected DbSet<T> DbSet
        {
            get
            {
                return context.Set<T>();
            }
        }

        public Repository()
        {
            context = new NordhusContext();
            sharedContext = false;
        }

        public Repository(NordhusContext context)
        {
            this.context = context;
            sharedContext = true;
        }

        public virtual void Create(T entity)
        {
            DbSet.Add(entity);
            if (!sharedContext)
            {
                context.SaveChanges();
            }
        }

        public virtual void Delete(T entity)
        {
            DbSet.Remove(entity);
            if (!sharedContext)
            {
                context.SaveChanges();
            }
        }

        public virtual void Update(T entity)
        {
            var oldEntity = context.Entry(entity);
            DbSet.Attach(entity);
            oldEntity.State = EntityState.Modified;
            if (!sharedContext)
            {
                context.SaveChanges();
            }
        }

        public virtual IQueryable<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public virtual List<T> Read()
        {
            return DbSet.ToList();
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            if (!sharedContext)
            {
                context.Dispose(); 
            }
        }

        ~Repository()
        {
            Dispose(false);
        } 
        #endregion
    }
}
