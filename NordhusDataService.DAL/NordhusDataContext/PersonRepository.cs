﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;

namespace NordhusDataService.DAL
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(NordhusContext context)
            : base(context)
        {
        }

        public Person FindById(int id)
        {
            return context.People.Find(id);
        }
    }
}
