﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;

namespace NordhusDataService.DAL
{
    public class ApartmentRepository : Repository<Apartment>, IApartmentRepository
    {
        public ApartmentRepository(NordhusContext context)
            : base(context)
        {
        }

        public Apartment FindById(int id)
        {
            return context.Apartments.Include("Contracts").FirstOrDefault(o => o.ApartmentId == id);
        }

        public List<Apartment> GetAll()
        {
            return DbSet.Include("Contracts").ToList();
        }

        public List<Apartment> GetAvailable()
        {
            return DbSet.Include("Contracts").Where(o => o.Contracts.FirstOrDefault(i => i.EndDate == null) == null).ToList();
        }

        public List<Apartment> GetNotAvailable()
        {
            return DbSet.Include("Contracts").Where(o => o.Contracts.FirstOrDefault(i => i.EndDate == null) != null).ToList();
        }
    }
}
