﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;

namespace NordhusDataService.DAL
{
    public interface IApartmentRepository : IRepository<Apartment>
    {
        Apartment FindById(int id);
        List<Apartment> GetAll();
        List<Apartment> GetAvailable();
        List<Apartment> GetNotAvailable();
    }
}
