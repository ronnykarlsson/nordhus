﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace NordhusDataService.DAL
{
    public interface IRepository<T> : IDisposable
        where T : class
    {
        void Create(T entity);
        List<T> Read();
        void Update(T entity);
        void Delete(T entity);
        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
    }
}
