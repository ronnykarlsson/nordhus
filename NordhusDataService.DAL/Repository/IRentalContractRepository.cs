﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;

namespace NordhusDataService.DAL
{
    public interface IRentalContractRepository : IRepository<RentalContract>
    {
        RentalContract FindById(int id);
    }
}
