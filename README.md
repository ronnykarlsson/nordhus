# Nordhus

## Startup

To run Nordhus you need to start both the Nordhus project (WPF client) and NordhusServiceConsole project (WCF service from console application).

1. Right-click solution
2. Properties
3. Choose Multiple startup projects
4. Select Start for Nordhus and NordhusServiceConsole
