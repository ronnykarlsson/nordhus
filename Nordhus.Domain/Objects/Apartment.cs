﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Nordhus.Domain
{
    [DataContract(IsReference=true)]
    public class Apartment
    {
        public Apartment() { }

        public Apartment(int apartmentId, int number, Address address)
        {
            ApartmentId = apartmentId;
            Number = number;
            Address = address;
        }

        [DataMember]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ApartmentId { get; set; }
        [DataMember]
        [Required]
        public int Number { get; set; }
        [DataMember]
        public Address Address { get; set; }
        [DataMember]
        public List<RentalContract> Contracts { get; set; }

        public RentalContract CurrentContract
        {
            get
            {
                if (Contracts == null) return null;
                return Contracts.FirstOrDefault(o => o.EndDate == null);
            }
        }
    }
}
