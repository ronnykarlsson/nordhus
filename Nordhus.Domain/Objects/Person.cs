﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Nordhus.Domain
{
    [DataContract(IsReference=true)]
    public class Person
    {
        public Person() { }

        public Person(int personId, string firstName, string lastName, string socialSecurityNumber)
        {
            PersonId = personId;
            FirstName = firstName;
            LastName = lastName;
            SocialSecurityNumber = socialSecurityNumber;
        }

        [DataMember]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PersonId { get; set; }
        [DataMember]
        [Required, StringLength(30)]
        public string FirstName { get; set; }
        [DataMember]
        [Required, StringLength(30)]
        public string LastName { get; set; }
        [DataMember]
        [Required, StringLength(20)]
        public string SocialSecurityNumber { get; set; }
        [DataMember]
        public List<RentalContract> RentalContract { get; set; }



        public override bool Equals(object obj)
        {
            Person person = obj as Person;

            if (person == null)
            {
                return false;
            }

            if (PersonId == person.PersonId
                && String.CompareOrdinal(FirstName, person.FirstName) == 0
                && String.CompareOrdinal(LastName, person.LastName) == 0
                && String.CompareOrdinal(SocialSecurityNumber, person.SocialSecurityNumber) == 0)
            {
                return true;
            }
            else
            {
                // At least one value don't match
                return false;
            }
        }
    }
}
