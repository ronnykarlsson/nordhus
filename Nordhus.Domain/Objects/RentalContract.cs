﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Nordhus.Domain
{
    [DataContract(IsReference=true)]
    public class RentalContract : IBusinessRules<RentalContract>
    {
        public RentalContract() { }

        public RentalContract(int contractId, int apartmentId, List<Person> renter, DateTime startDate, DateTime? endDate)
        {
            RentalContractId = contractId;
            ApartmentId = apartmentId;
            Renter = renter;
            StartDate = startDate;
            EndDate = endDate;
        }

        [DataMember]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RentalContractId { get; set; }
        [DataMember]
        [Required]
        public int ApartmentId { get; set; }
        [DataMember]
        public List<Person> Renter { get; set; }
        [DataMember]
        [Required]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember, ForeignKey("ApartmentId")]
        public Apartment Apartment { get; set; }


        public ValidationResult IsValid()
        {
            List<string> errorMessages = new List<string>();

            if (EndDate <= StartDate)
                errorMessages.Add("EndDate must be greater than StartDate");

            if (Renter == null || Renter.Count <= 0)
                errorMessages.Add("At least one person is required to create the contract");

            if (ApartmentId < 0)
                errorMessages.Add("Invalid ApartmentId");

            bool isValid = errorMessages.Count == 0 ? true : false;

            return new ValidationResult(isValid, errorMessages);
        }

        public ValidationResult IsValidUpdate(RentalContract entity)
        {
            List<string> errorMessages = new List<string>();

            if (RentalContractId != entity.RentalContractId)
                errorMessages.Add("RentalContractId doesn't match.");

            if (ApartmentId != entity.ApartmentId)
                errorMessages.Add("Can't change ApartmentId on existing contract.");

            if (!CheckRentersMatch(entity))
                errorMessages.Add("Can't change renter on existing contract.");

            if (entity.EndDate != null && entity.EndDate <= entity.StartDate)
                errorMessages.Add("EndDate can't precede StartDate");

            bool isValid = errorMessages.Count == 0 ? true : false;

            return new ValidationResult(isValid, errorMessages);
        }

        public void Update(RentalContract entity)
        {
            var validation = IsValidUpdate(entity);
            if (!validation.IsValid)
            {
                throw new InvalidOperationException("Invalid update");
            }

            this.StartDate = entity.StartDate;
            this.EndDate = entity.EndDate;
        }

        public override bool Equals(object obj)
        {
            RentalContract contract = obj as RentalContract;

            if (contract == null)
            {
                return false;
            }

            // If two DateTimes are the same down to a second, they'll be considered equal
            // This is to be sure we don't get into trouble with rounding errors

            DateTime date1 = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, StartDate.Hour, StartDate.Minute, StartDate.Second);
            DateTime date2 = new DateTime(contract.StartDate.Year, contract.StartDate.Month, contract.StartDate.Day,
                contract.StartDate.Hour, contract.StartDate.Minute, contract.StartDate.Second);

            if (date1 != date2)
            {
                return false;
            }

            if (EndDate != null && contract.EndDate != null)
            {
                date1 = new DateTime(EndDate.Value.Year, EndDate.Value.Month, EndDate.Value.Day, EndDate.Value.Hour, EndDate.Value.Minute, EndDate.Value.Second);
                date2 = new DateTime(contract.EndDate.Value.Year, contract.EndDate.Value.Month, contract.EndDate.Value.Day,
                    contract.EndDate.Value.Hour, contract.EndDate.Value.Minute, contract.EndDate.Value.Second);

                if (date1 != date2)
                {
                    return false;
                }
            }

            if (!CheckRentersMatch(contract)) return false;

            // Everything passed, contracts are equal
            return true;
        }

        private bool CheckRentersMatch(RentalContract contract)
        {
            if (RentalContractId == contract.RentalContractId
                && ApartmentId == contract.ApartmentId)
            {

                if (Renter == null && contract.Renter == null)
                {
                    // Both values being null means they are equal
                    return true;
                }

                if (Renter == null || contract.Renter == null)
                {
                    // One of renter-lists can't be null
                    return false;
                }

                if (Renter.Count != contract.Renter.Count)
                {
                    // If both lists have different lengths then they can't be equal
                    return false;
                }

                for (int i = 0; i < Renter.Count; i++)
                {
                    if (!Renter[i].Equals(contract.Renter[i]))
                    {
                        // If any value doesn't match up, then the contracts don't match
                        return false;
                    }
                }
            }
            else // One of the contracts members aren't equal, return false
            {
                return false;
            }
            return true;
        }
    }
}
