﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Nordhus.Domain
{
    [DataContract]
    public class Address
    {
        public Address() { }

        public Address(string street, int postalCode, string city, string area)
        {
            Street = street;
            PostalCode = postalCode;
            City = city;
            Area = area;
        }

        [DataMember]
        [Required, StringLength(30)]
        public string Street { get; set; }
        [DataMember]
        [Required, Range(1000, 99999)]
        public int PostalCode { get; set; }
        [DataMember]
        [Required, StringLength(30)]
        public string City { get; set; }
        [DataMember]
        [StringLength(30)]
        public string Area { get; set; }
    }
}
