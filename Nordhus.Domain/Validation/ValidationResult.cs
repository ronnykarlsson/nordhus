﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nordhus.Domain
{
    public class ValidationResult
    {
        public bool IsValid { get; private set; }
        public IEnumerable<string> ErrorMessages { get; private set; }

        public ValidationResult(bool isValid, IEnumerable<string> errorMessages)
        {
            IsValid = isValid;
            ErrorMessages = errorMessages;
        }
    }
}
