﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nordhus.Domain
{
    public interface IBusinessRules<T>
    {
        ValidationResult IsValid();
        ValidationResult IsValidUpdate(T entity);
        void Update(T entity);
    }
}
