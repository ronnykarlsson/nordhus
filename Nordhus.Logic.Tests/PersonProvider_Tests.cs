﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Nordhus.Domain;
using NordhusDataService;
using System.ServiceModel;

namespace Nordhus.Logic.Tests
{
    [TestFixture]
    public class PersonProvider_Tests
    {
        PersonProvider provider;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
        }

        [SetUp]
        public void TestSetUp()
        {
            provider = new PersonProvider(new NordhusDataServiceMock());
        }

        [Test]
        public void GetCanRetrievePerson()
        {
            Person person = provider.Get(1);
            Assert.AreEqual(person.PersonId, 1);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDoesntAskServiceForInvalidIds()
        {
            provider.Get(-1);
        }

        [Test]
        public void GetAllFindsListOfAtLeastTwoPeople()
        {
            List<Person> list = provider.GetAll();
            Assert.GreaterOrEqual(list.Count, 2);
        }
    }
}
