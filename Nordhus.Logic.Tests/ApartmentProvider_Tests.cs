﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Nordhus.Domain;
using NordhusDataService;
using System.ServiceModel;

namespace Nordhus.Logic.Tests
{
    [TestFixture]
    public class ApartmentProvider_Tests
    {
        ApartmentProvider provider;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
        }

        [SetUp]
        public void TestSetUp()
        {
            provider = new ApartmentProvider(new NordhusDataServiceMock());
        }

        [Test]
        public void GetCanRetrieveAnApartment()
        {
            Apartment apartment = provider.Get(1);
            Assert.AreEqual(1, apartment.ApartmentId);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDoesntAskForInvalidIds()
        {
            provider.Get(-1);
        }

        [Test]
        public void GetAllFindsAtLeast5Apartments()
        {
            List<Apartment> apartments = provider.GetAll();
            Assert.GreaterOrEqual(apartments.Count, 5);
        }
    }
}
