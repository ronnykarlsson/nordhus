﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;
using System.ServiceModel;

namespace Nordhus.Logic.Tests
{
    public class NordhusDataServiceMock : IClientProxy
    {
        NordhusDataService.NordhusDataService service;

        public NordhusDataServiceMock()
        {
            service = new NordhusDataService.NordhusDataService();
        }

        public Person GetPersonById(int personId)
        {
            return service.GetPersonById(personId);
        }

        public List<Person> GetAllPeople()
        {
            return service.GetAllPeople();
        }

        public List<Person> SearchPerson(string searchString)
        {
            return service.SearchPerson(searchString);
        }

        public Apartment GetApartmentById(int apartmentId)
        {
            return service.GetApartmentById(apartmentId);
        }

        public List<Apartment> GetAllApartments()
        {
            return service.GetAllApartments();
        }

        public List<Apartment> GetAvailableApartments()
        {
            return service.GetAvailableApartments();
        }

        public List<Apartment> GetNotAvailableApartments()
        {
            return service.GetNotAvailableApartments();
        }

        public List<Apartment> SearchApartment(string searchString)
        {
            return service.SearchApartment(searchString);
        }

        public RentalContract GetContractById(int contractId)
        {
            return service.GetContractById(contractId);
        }

        public List<RentalContract> GetAllContracts()
        {
            return service.GetAllContracts();
        }

        public List<RentalContract> SearchContract(string searchString)
        {
            return service.SearchContract(searchString);
        }

        public int CreateContract(RentalContract contract)
        {
            return service.CreateContract(contract);
        }

        public void UpdateContract(RentalContract contract)
        {
            service.UpdateContract(contract);
        }

        public void CloseContract(int contractId)
        {
            service.CloseContract(contractId);
        }

        public void Abort()
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginClose(TimeSpan timeout, AsyncCallback callback, object state)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginClose(AsyncCallback callback, object state)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginOpen(TimeSpan timeout, AsyncCallback callback, object state)
        {
            throw new NotImplementedException();
        }

        public IAsyncResult BeginOpen(AsyncCallback callback, object state)
        {
            throw new NotImplementedException();
        }

        public void Close(TimeSpan timeout)
        {
            throw new NotImplementedException();
        }

        public void Close()
        {
        }

        public event EventHandler Closed;

        public event EventHandler Closing;

        public void EndClose(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public void EndOpen(IAsyncResult result)
        {
            throw new NotImplementedException();
        }

        public event EventHandler Faulted;

        public void Open(TimeSpan timeout)
        {
            throw new NotImplementedException();
        }

        public void Open()
        {
            throw new NotImplementedException();
        }

        public event EventHandler Opened;

        public event EventHandler Opening;

        public CommunicationState State
        {
            get { return CommunicationState.Opened; }
        }
    }
}
