﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Nordhus.Domain;
using System.ServiceModel;
using Autofac;
using System.Data.Entity;
using NordhusDataService.DAL;

namespace Nordhus.Logic.Tests
{
    [TestFixture]
    public class RentalContractProvider_Tests
    {
        RentalContractProvider provider;
        PersonProvider personProvider;
        ContainerBuilder builder;
        IContainer container;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            // Initialize database with the same test data each time
            Database.SetInitializer<NordhusContext>(new TestDatabaseInitializer());
        }

        [SetUp]
        public void TestSetUp()
        {
            provider = new RentalContractProvider(new NordhusDataServiceMock());
            personProvider = new PersonProvider(new NordhusDataServiceMock());

            // Delete previous database to be sure we start up with a fresh one
            using (NordhusContext context = new NordhusContext())
            {
                context.Database.Delete();
                context.Database.Initialize(true);
            }
        }

        [Test]
        public void GetCanRetrieveAContract()
        {
            CreateContractHelper();
            var contract = provider.Get(1);
            Assert.AreEqual(1, contract.RentalContractId);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetDoesntAskServiceForInvalidIds()
        {
            provider.Get(-1);
        }

        [Test]
        public void GetAllFindsAtLeastOneContract()
        {
            CreateContractHelper();
            var contracts = provider.GetAll();
            Assert.GreaterOrEqual(contracts.Count, 1);
        }

        [Test]
        public void CreateContractReturnsIdOfNewContract()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            provider.CreateContract(contract);
            Assert.GreaterOrEqual(contract.RentalContractId, 0);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateContractDoesNotAllowEndDatesFromBeforeStartDate()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(-30));
            provider.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateContractStartDateAndEndDateCantBeSame()
        {
            DateTime now = DateTime.UtcNow;
            var contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, now, now);
            provider.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateContractFailsWithNoRenters()
        {
            var contract = new RentalContract(-1, 1, new List<Person> { }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            provider.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateContractFailsWithBadPersonArgument()
        {
            var contract = new RentalContract(-1, 1, null, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            provider.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateContractDoesntTakeNegativeApartmentId()
        {
            var contract = new RentalContract(-1, -1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            provider.CreateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void CreateContractOnlyTakesNegativeContractIds()
        {
            var contract = new RentalContract(0, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            provider.CreateContract(contract);
        }

        [Test]
        public void CreatedContractIsPersistedAndRetrievedCorrectly()
        {
            var contract1 = CreateContractHelper();
            var contract2 = provider.Get(contract1.RentalContractId);
            Assert.IsTrue(contract1.Equals(contract2));
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void UpdateContractWithInvalidIdGivesFaultException()
        {
            var contract = CreateContractHelper();
            contract.RentalContractId = -1;
            provider.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void UpdateContractDoesntAllowStartDateAndDateToBeSame()
        {
            CreateContractHelper();
            var contract = provider.Get(1);
            contract.EndDate = contract.StartDate;
            provider.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void UpdateContractRequiresEndDateGreaterThanStartDate()
        {
            CreateContractHelper();
            var contract = provider.Get(1);
            contract.EndDate = contract.StartDate.AddDays(-1);
            provider.UpdateContract(contract);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void UpdateContractDoesntAllowNegativeContractIds()
        {
            CreateContractHelper();
            var contract = provider.Get(1);
            contract.ApartmentId = -1;
            provider.UpdateContract(contract);
        }

        [Test]
        public void UpdateContractChangesArePersistedAndCanBeRetrieved()
        {
            var contract1 = CreateContractHelper();
            contract1.StartDate = contract1.StartDate.AddDays(1);
            contract1.EndDate = contract1.EndDate.Value.AddDays(1);
            provider.UpdateContract(contract1);
            var contract2 = provider.Get(contract1.RentalContractId);
            Assert.IsTrue(contract1.Equals(contract2));
        }

        private RentalContract CreateContractHelper()
        {
            RentalContract contract;
            contract = new RentalContract(-1, 1, new List<Person> { GetPersonHelper() }, DateTime.UtcNow, DateTime.UtcNow.AddDays(30));
            provider.CreateContract(contract);
            return contract;
        }

        private Person GetPersonHelper()
        {
            return personProvider.Get(1);
        }
    }
}
