﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace NordhusServiceConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(NordhusDataService.NordhusDataService)))
            {
                host.Open();
                Console.ReadLine();
            }
        }
    }
}
