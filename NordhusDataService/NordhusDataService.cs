﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nordhus.Domain;
using NordhusDataService.DAL;
using System.Data.Entity;
using System.ServiceModel;
using Autofac;
using log4net;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;

namespace NordhusDataService
{
    [NordhusBehavior]
    public partial class NordhusDataService : INordhusDataService
    {
        ContainerBuilder builder;
        IContainer container;

        public NordhusDataService()
        {
            builder = new ContainerBuilder();
            builder.RegisterType<NordhusDataContext>().As<IDataAccessLayer>();
            container = builder.Build();

            // Put dummy-data in the database
            Database.SetInitializer<NordhusContext>(new NordhusDatabaseInitializer());
        }

        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Get information about the current call for logging purposes.
        /// </summary>
        /// <returns>Call information</returns>
        private string GetCallInformation()
        {
            return "called";
        }

        /// <summary>
        /// Find a person with specified ID
        /// </summary>
        /// <param name="personId">ID to look for</param>
        /// <returns>Person object</returns>
        public Person GetPersonById(int personId)
        {
            log.Info(GetCallInformation());

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                var person = dal.People.FindById(personId);
                if (person == null)
                {
                    log.ErrorFormat("Person {0} not found", personId);
                    throw new FaultException("Person with specified ID wasn't found.");
                }
                else
                {
                    return person;
                }
            }
        }

        /// <summary>
        /// Retrieve all people from the database
        /// </summary>
        /// <returns>List of all people</returns>
        public List<Person> GetAllPeople()
        {
            log.Info(GetCallInformation());

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                return dal.People.Read();
            }
        }

        /// <summary>
        /// Return list of people matching the string
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public List<Person> SearchPerson(string searchString)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieve apartment with the specified ID
        /// </summary>
        /// <param name="apartmentId">ID of apartment to look for</param>
        /// <returns>Apartment object</returns>
        public Apartment GetApartmentById(int apartmentId)
        {
            log.Info(GetCallInformation());

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                var apartment = dal.Apartments.FindById(apartmentId);
                if (apartment == null)
                {
                    log.ErrorFormat("Apartment {0} not found", apartmentId);

                    throw new FaultException("No apartment with specified ID was found.");
                }
                else
                {
                    return apartment;
                }
            }
        }

        /// <summary>
        /// Retrieve a list of all apartments in the database
        /// </summary>
        /// <returns>List of all apartments</returns>
        public List<Apartment> GetAllApartments()
        {
            log.Info(GetCallInformation());

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                return dal.Apartments.GetAll();
            }
        }

        /// <summary>
        /// Retrieve a list of all apartments in the database
        /// </summary>
        /// <returns>List of all apartments</returns>
        public List<Apartment> GetAvailableApartments()
        {
            log.Info(GetCallInformation());

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                return dal.Apartments.GetAvailable();
            }
        }

        /// <summary>
        /// Retrieve a list of all apartments in the database
        /// </summary>
        /// <returns>List of all apartments</returns>
        public List<Apartment> GetNotAvailableApartments()
        {
            log.Info(GetCallInformation());

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                return dal.Apartments.GetNotAvailable();
            }
        }

        /// <summary>
        /// Search for apartments
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public List<Apartment> SearchApartment(string searchString)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieve rental contract with the specified Id
        /// </summary>
        /// <param name="contractId">Contract ID to look for</param>
        /// <returns>RentalContract object</returns>
        public RentalContract GetContractById(int contractId)
        {
            log.Info(GetCallInformation());

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                RentalContract contract;
                try
                {
                    contract = dal.RentalContracts
                                    .Find(o => o.RentalContractId == contractId)
                                    .Include(o => o.Renter)
                                    .First();
                }
                catch (InvalidOperationException)
                {
                    log.ErrorFormat("RentalContract {0} not found", contractId);
                    throw new FaultException("Rental contract with specified ID wasn't found.");
                }
                return contract;
            }
        }

        /// <summary>
        /// Retrieve a list of all contracts from the database
        /// </summary>
        /// <returns>List of all contracts</returns>
        public List<RentalContract> GetAllContracts()
        {
            log.Info(GetCallInformation());

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                return dal.RentalContracts.Read();
            }
        }

        /// <summary>
        /// Search for contract
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public List<RentalContract> SearchContract(string searchString)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Save a contract to the database
        /// </summary>
        /// <param name="contract">Contract to be saved</param>
        public int CreateContract(RentalContract contract)
        {
            log.Info(GetCallInformation());

            if (contract.RentalContractId >= 0)
            {
                log.ErrorFormat("Invalid contract ID {0}", contract.RentalContractId);
                throw new FaultException("Invalid RentalContractId");
            }

            ValidationResult result = contract.IsValid();
            if (!result.IsValid)
            {
                log.ErrorFormat("Contract didn't pass validation: {0}", result.ErrorMessages.First());
                throw new FaultException("Invalid contract parameters");
            }

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                bool noMissingPeople = contract.Renter.All(o => dal.People.FindById(o.PersonId) != null);
                if (!noMissingPeople)
                {
                    log.Error("Invalid PersonId");
                    throw new FaultException("Invalid PersonId");
                }

                Apartment apartment = dal.Apartments.FindById(contract.ApartmentId);
                if (apartment == null)
                {
                    log.Error("Invalid ApartmentId");
                    throw new FaultException("Invalid ApartmentId");
                }

                try
                {
                    VerifyContractDates(contract, apartment);
                }
                catch (FaultException ex)
                {
                    log.ErrorFormat("Invalid contract dates: ", ex.Message);
                    throw;
                }
            }

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
        	{
                dal.RentalContracts.Create(contract);
                dal.SaveChanges();
                return contract.RentalContractId;
            }
        }

        private static void VerifyContractDates(RentalContract contract, Apartment apartment)
        {
            const string badParameterString = "Invalid {0}. Please refer to contract {1}";
            foreach (var apartmentContract in apartment.Contracts)
            {
                // If this is a contract update, we don't want to check against the old one
                if (contract.RentalContractId == apartmentContract.RentalContractId) continue;

                if (apartmentContract.EndDate == null)
                {
                    // If there is an open contract, the created contract must have an
                    // EndDate earlier than this contracts StartDate
                    if (contract.EndDate == null || contract.EndDate >= apartmentContract.StartDate)
                    {
                        throw new FaultException(String.Format(badParameterString, "EndDate", apartmentContract.RentalContractId));
                    }
                }
                else if (apartmentContract.StartDate <= contract.EndDate
                    && apartmentContract.EndDate >= contract.EndDate)
                {
                    // EndDate must be outside of apartmentContracts period
                    throw new FaultException(String.Format(badParameterString, "EndDate", apartmentContract.RentalContractId));
                }
                else if (apartmentContract.StartDate <= contract.StartDate
                    && apartmentContract.EndDate >= contract.StartDate)
                {
                    // StartDate must be outside of apartmentContracts period
                    throw new FaultException(String.Format(badParameterString, "StartDate", apartmentContract.RentalContractId));
                }
            }
        }

        /// <summary>
        /// Modify a contract and save to database
        /// </summary>
        /// <param name="contract">The new contract information</param>
        public void UpdateContract(RentalContract contract)
        {
            log.Info(GetCallInformation());

            if (contract.RentalContractId < 0)
            {
                log.ErrorFormat("Invalid RentalContractId {0}", contract.RentalContractId);

                throw new FaultException("Invalid RentalContractId");
            }

            using (IDataAccessLayer dal = container.Resolve<IDataAccessLayer>())
            {
                RentalContract updateContract = dal.RentalContracts.FindById(contract.RentalContractId);
                if (updateContract == null)
                {
                    // Unable to retrieve the contract, probably wasn't found
                    // Signal it as an invalid id since it's an update method
                    log.Error("Invalid RentalContractId");
                    throw new FaultException("Invalid RentalContractId");
                }

                var validation = updateContract.IsValidUpdate(contract);
                if (!validation.IsValid)
                {
                    log.ErrorFormat("Invalid contract update: {0}", validation.ErrorMessages.First());
                    throw new FaultException("Validation error");
                }

                var apartment = dal.Apartments.FindById(contract.ApartmentId);
                if (apartment == null)
                {
                    log.Error("Invalid ApartmentId");
                    throw new FaultException("Invalid ApartmentId");
                }

                try
                {
                    VerifyContractDates(contract, apartment);
                }
                catch (FaultException ex)
                {
                    log.ErrorFormat("Invalid contract dates: {0}", ex.Message);
                    throw;
                }

                updateContract.Update(contract);

                dal.SaveChanges();
            }
        }

        /// <summary>
        /// Close contract
        /// </summary>
        /// <param name="contractId">ID of contract to close</param>
        public void CloseContract(int contractId)
        {
            throw new NotImplementedException();
        }
    }
}
