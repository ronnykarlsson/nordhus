﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Nordhus.Domain;

namespace NordhusDataService
{
    [ServiceContract]
    public interface INordhusDataService
    {
        // Person methods
        [OperationContract]
        Person GetPersonById(int personId);

        [OperationContract]
        List<Person> GetAllPeople();

        [OperationContract]
        List<Person> SearchPerson(string searchString);


        // Apartment methods
        [OperationContract]
        Apartment GetApartmentById(int apartmentId);

        [OperationContract]
        List<Apartment> GetAllApartments();

        [OperationContract]
        List<Apartment> GetAvailableApartments();

        [OperationContract]
        List<Apartment> GetNotAvailableApartments();

        [OperationContract]
        List<Apartment> SearchApartment(string searchString);


        // Rental contract method
        [OperationContract]
        RentalContract GetContractById(int contractId);

        [OperationContract]
        List<RentalContract> GetAllContracts();

        [OperationContract]
        List<RentalContract> SearchContract(string searchString);

        [OperationContract]
        int CreateContract(RentalContract contract);

        [OperationContract]
        void UpdateContract(RentalContract contract);

        [OperationContract]
        void CloseContract(int contractId);
    }
}
