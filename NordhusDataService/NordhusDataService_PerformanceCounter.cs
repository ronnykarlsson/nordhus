﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace NordhusDataService
{
    public partial class NordhusDataService
    {
        private static readonly string PerformanceCounterCategoryName = "NordhusDataService";
        private static readonly string PerformanceCounterTotalCallsName = "total calls";

        private static readonly PerformanceCounter totalCallsPerformanceCounter;

        static NordhusDataService()
        {
            try
            {
                // Create performance counter category
                if (!PerformanceCounterCategory.Exists(PerformanceCounterCategoryName))
                {
                    CounterCreationDataCollection counters = new CounterCreationDataCollection();

                    counters.Add(new CounterCreationData(PerformanceCounterTotalCallsName, "Total number of service calls made", PerformanceCounterType.NumberOfItems32));

                    PerformanceCounterCategory.Create(PerformanceCounterCategoryName, "Nordhus Service counters", PerformanceCounterCategoryType.SingleInstance, counters);
                }
            }
            catch (System.IO.IOException)
            {
            }

            // Get PerformanceCounter instances
            totalCallsPerformanceCounter = new PerformanceCounter(PerformanceCounterCategoryName, PerformanceCounterTotalCallsName, false);
        }

        /// <summary>
        /// Increase total calls performance counter by one
        /// </summary>
        public static void IncreaseTotalCallsCounter()
        {
            totalCallsPerformanceCounter.Increment();
        }
    }
}
